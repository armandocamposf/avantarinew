<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Catalogs\OperationType;

class ItemCashInventory extends ModelTenant
{
    protected $table = 'items_cash_inventory';

    protected $fillable = [
        'cash_id',
        'item_id',
        'realized',
        'observation',
        'inicio',
        'final'
    ];

    protected $casts = [
        'realized' => 'boolean',
    ];


    public function cash()
    {
        return $this->belongsTo(Cash::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

}
