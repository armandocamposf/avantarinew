<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Models\Tenant\Document;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;



class UploadFileController extends Controller
{

    public function uploadOcInvoice(Request $request)
    {
        
            $document_id = $request->document_id;
            $temp_path = $request->temp_path_oc;

            if ($temp_path) {

                $directory = 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'invoice-oc' . DIRECTORY_SEPARATOR;

                $file_name_old = $request->input('file_name_oc');
                $file_name_old_array = explode('.', $file_name_old);
                $file_content = file_get_contents($temp_path);
                $datenow = date('YmdHis');
                $file_name = Str::slug($file_name_old) . '-' . $datenow . '.' . $file_name_old_array[1];

                Storage::put($directory . $file_name, $file_content);
                Document::where('id', $document_id)->update(['file_oc_url' => $file_name] );
            }

            return [
                'success' => true,
                'message' => 'Oc invoice guardada con exito.'
            ];

        }
        
}
