<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="application/pdf; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte POS - Cierre Inventario
    </title>
    <style>
        body {
            font-family: sans-serif;
            font-size: 12px;
        }

        table {
            width: 100%;
            border-spacing: 0;
            border: 1px solid black;
        }

        .celda {
            text-align: center;
            padding: 5px;
            border: 0.1px solid black;
        }

        th {
            padding: 5px;
            text-align: center;
            border: 0.1px solid #0088cc;
        }

        .title {
            font-weight: bold;
            padding: 5px;
            font-size: 20px !important;
            text-decoration: underline;
        }

        p > strong {
            margin-left: 5px;
            font-size: 12px;
        }

        thead tr th {
            font-weight: bold;
            background: #0088cc;
            color: white;
            text-align: center;
        }

        .width-custom {
            width: 50%
        }
    </style>
</head>
<body>
    <div class="">
        <p><strong> Inventario </strong> </p>
        <div class=" ">
            <table>
                <thead>
                <tr>
                    <th>
                        Cod Interno
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Stock
                    </th>
                    <th>
                        Inventariado
                    </th>
                    <th>
                        Diferencia
                    </th>
                    <th>
                        Observaciones
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($caja[0]->items as $item)
                    <tr>
                        <td class="celda">
                            {{ $item->item->internal_id }}
                        </td>
                        <td class="celda">
                            {{ $item->item->name }}
                        </td>
                        <td class="celda">
                            {{ $item['inicio'] }}
                        </td>
                        <td class="celda">
                            {{ $item['final'] }}
                        </td>
                        <td class="celda">
                            {{ $item['inicio'] - $item['final'] }}
                        </td>
                        <td class="celda">
                            {{ $item['observation'] }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>
